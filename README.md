# My course DevOps-School project! 

1. Build front     :heavy_check_mark:
2. Build back-end  :heavy_check_mark:
3. Dockerize front :heavy_check_mark:
4. Docker-compose  :heavy_check_mark:
5. HELM CHART :heavy_check_mark:
6. VERSIONING :heavy_check_mark:

### Information

##### Front-app

URL: hostname:8081/dev-ops-school/index.html   

*NEEDS ENV: Backend-hostname*

##### Backend

URL: hostname:8080/swagger-ui.html

*Needs ENV*:

- **POSTGRES_USER**
- **POSTGRES_PASSWORD**
- **POSTGRES_DB**: dev-school

- DB_SERVER *HOSTNAME* 




