#!/bin/bash
COMMIT=$1
ver=$(echo $COMMIT | cut -f1 -d:)

 if [[ $COMMIT == *"BREAKING CHANGE"* ]]; then
   echo "nextMajor "
 elif  [[ "$ver" == "feat" ]]; then
   echo "nextMinor"
 elif [[ "$ver" == "fix" ]]; then
   echo "nextPatch"
 else
  echo "Nothing new: commit message type wrong specicfy!"
 fi
